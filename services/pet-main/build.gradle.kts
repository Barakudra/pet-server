import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
	id("org.springframework.boot") version "2.5.3"
	id("io.spring.dependency-management") version "1.0.11.RELEASE"
	war
	kotlin("jvm") version "1.5.21"
	kotlin("plugin.spring") version "1.5.21"
	kotlin("plugin.jpa") version "1.5.21"
	id("io.swagger.core.v3.swagger-gradle-plugin") version "2.1.10"
}

group = "by.birukov"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_16

repositories {
	mavenCentral()
}

dependencies {
	implementation("org.springframework.boot:spring-boot-starter-data-jpa")
	implementation("org.springframework.boot:spring-boot-starter-security")
	implementation("org.springframework.boot:spring-boot-starter-web")
	implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
	implementation("org.jetbrains.kotlin:kotlin-reflect")
	implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
	implementation("io.swagger.core.v3:swagger-annotations:2.1.10")
	implementation("org.springdoc:springdoc-openapi-ui:1.5.10")
	implementation("org.springdoc:springdoc-openapi-kotlin:1.5.10")
	runtimeOnly("org.postgresql:postgresql")
	providedRuntime("org.springframework.boot:spring-boot-starter-tomcat")
	testImplementation("org.springframework.boot:spring-boot-starter-test")
	implementation("org.liquibase:liquibase-core:4.4.2")
	implementation("org.modelmapper:modelmapper:2.4.4")
	testImplementation("org.jetbrains.kotlin:kotlin-test-junit5:1.5.30-RC")
	testImplementation("io.mockk:mockk:1.12.0")
	implementation("javax.validation:validation-api:2.0.1.Final")
	implementation("org.hibernate.validator:hibernate-validator:7.0.1.Final")
	testImplementation("org.springframework.security:spring-security-test")
	implementation("org.springframework.cloud:spring-cloud-starter-openfeign:3.0.3")
	implementation("org.springframework.cloud:spring-cloud-starter-netflix-hystrix:2.2.9.RELEASE")
	implementation("org.springframework.cloud:spring-cloud-starter-netflix-eureka-server:3.0.4")
	implementation("com.sun.jersey.contribs:jersey-apache-client4:1.19.4")
	implementation("com.auth0:java-jwt:3.18.2")
}

tasks.withType<KotlinCompile> {
	kotlinOptions {
		freeCompilerArgs = listOf("-Xjsr305=strict")
		jvmTarget = "16"
	}
}

tasks.withType<Test> {
	useJUnitPlatform()
}

allOpen {
	annotation("javax.persistence.Entity")
	annotation("javax.persistence.MappedSuperclass")
	annotation("javax.persistence.Embeddable")
}