package by.birukov.pet.controller

import by.birukov.pet.dto.BasicVehicleDto
import by.birukov.pet.dto.VehicleDetailsDto
import by.birukov.pet.service.VehicleService
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.responses.ApiResponses
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

/**
 * @author Birukov
 * Provide functional work with vehicles
 */
@Tag(name = "Vehicle", description = "The vehicle API.")

@RestController
@RequestMapping("/cars")
class VehicleController(private val vehicleService: VehicleService) {

    /** Return list of [BasicVehicleDto] for the simple view */
    @GetMapping

    @Operation(summary = "Get all vehicles.")
    @ApiResponses(ApiResponse(responseCode = "200", description = "Vehicles was received."))
    fun getAll(): List<BasicVehicleDto> = vehicleService.findAllUndeleted()

    /** Return [VehicleDetailsDto] by [id] */
    @Operation(summary = "Get details about vehicle.")
    @ApiResponses(ApiResponse(responseCode = "200", description = " Details about vehicle was received."))

    @GetMapping("/{id}")
    fun getById(@PathVariable id: Int): VehicleDetailsDto = vehicleService.getById(id)

    /** Create vehicle from [vehicleDetailsDto] */
    @Operation(summary = "Create vehicle.")
    @ApiResponses(
        ApiResponse(responseCode = "201", description = "Vehicle was created."),
        ApiResponse(responseCode = "409", description = "Register number not unique."),
    )

    @PostMapping
    fun create(@RequestBody vehicleDetailsDto: VehicleDetailsDto): VehicleDetailsDto = vehicleService.create(vehicleDetailsDto)

    /** Set entity in database with [id] as non-visible */
    @Operation(summary = "Delete vehicle.")
    @ApiResponses(ApiResponse(responseCode = "200", description = "Vehicle was deleted."),)

    @DeleteMapping("/{id}")
    fun delete(@PathVariable id: Int): VehicleDetailsDto = vehicleService.delete(id)
}