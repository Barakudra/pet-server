package by.birukov.pet.dto

import java.io.Serializable
import javax.validation.constraints.NotBlank
import javax.validation.constraints.PositiveOrZero

/** Copy of json from external service */
data class ExternalYearDto(

    @NotBlank
    val message: String,

    @PositiveOrZero
    val amount: Int,

    @NotBlank
    val type: String,

    @NotBlank
    val content: List<ExternalYearContentDto>
) : Serializable {

    companion object {
        private const val serialVersionUID: Long = 467356457678L
    }
}