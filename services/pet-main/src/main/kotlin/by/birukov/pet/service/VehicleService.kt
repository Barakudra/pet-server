package by.birukov.pet.service

import by.birukov.pet.dto.BasicVehicleDto
import by.birukov.pet.dto.VehicleDetailsDto
import by.birukov.pet.entity.Vehicle

interface VehicleService {

    /**
     * The method returns vehicle with detail description.
     *
     * @param [id] index of vehicle in database
     * @return [VehicleDetailsDto]
     */
    fun getById(id: Int): VehicleDetailsDto

    /**
     * The method returns list of existing entities in database, which are converted to simple view with little count of fields.
     *
     * @return [List(BasicVehicleDto)]
     */
    fun findAll(): List<BasicVehicleDto>

    /**
     * The method save new client in database
     *
     * @param [vehicleDetailsDto] the filled DTO, which will save
     */
    fun create(vehicleDetailsDto: VehicleDetailsDto): VehicleDetailsDto

    /**
     * The method mark vehicle as deleted
     *
     * This method finds entity in database by id, then set field "hidden" to value "true". It means, that entity will
     * not be visible.
     *
     * @param [id] - int value, id of marking vehicle.
     * @return [Vehicle] - deleting vehicle
     */
    fun delete(id: Int): VehicleDetailsDto

    /**
     * The method returns list vehicle with basic info about them
     *
     * The method returns list of entities in database, which not contain field "hidden" in the value "true",
     * which are converted to simple view with little count of fields.
     *
     * @return [MutableList(BasicClientDto)]
     */
    fun findAllUndeleted(): List<BasicVehicleDto>
}