package by.birukov.pet.dto

import java.io.Serializable
import java.time.LocalDate
import java.time.LocalDateTime
import javax.validation.constraints.Email
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull
import javax.validation.constraints.Past
import javax.validation.constraints.Size

/**
 * @author Birukov
 * @since 2021-09-06
 */
data class ClientDetailsDto(
    val id: Int = 0,

    @NotNull
    @NotEmpty
    @Email
    val email: String,

    @NotNull
    @Size(min = 6, max = 200)
    val password: String,

    @Past
    val birthDate: LocalDate,

    @Size(min = 3, max = 200)
    val firstName: String,

    @Size(min = 1, max = 100)
    val lastName: String
) : Serializable {

    lateinit var createdAt: LocalDateTime
    lateinit var updatedAt: LocalDateTime

    companion object {
        private const val serialVersionUID: Long = 2133400551086625355L
    }
}