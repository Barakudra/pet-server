package by.birukov.pet.enum

/** Available to the insurance parts of vehicle */
enum class PartOfVehicle {
    WHEELS,
    WIND_SCREEN,
    DOORS,
    ENGINE,
    HEADLIGHTS,
    BODYWORK,
    SUSPENSION
}