package by.birukov.pet.service

import by.birukov.pet.dto.ClientDetailsDto
import by.birukov.pet.dto.InsuranceContractDto

interface InsuranceContractService {

    /**
     * The method returns list of [InsuranceContractDto]. Show all insurance contracts. Any contract contain one or more
     * insurance kit
     *
     * @return [MutableList(InsuranceContractDto)]
     */
    fun getAll(): MutableList<InsuranceContractDto>

    /**
     * The method create [InsuranceContractDto] and inner objects.
     *
     * The method convert dto into entity and save in database.
     * @param [insuranceContractDto] dto with filling fields and at least one inner object.
     */
    fun create(insuranceContractDto: InsuranceContractDto): InsuranceContractDto

    /**
     * The method return contracts of chosen owner of vehicle.
     *
     * The method get id of client and search in database by id client. Then select all his vehicles from which it gets
     * all contracts, which  was registered at this client;
     *
     * @param [owner] - client, one of the owner of vehicle
     * @return [MutableList(InsuranceContractDto)] - list of contract, which client owns
     */
    fun getContractsOfOwner(owner: ClientDetailsDto): MutableList<InsuranceContractDto>
}