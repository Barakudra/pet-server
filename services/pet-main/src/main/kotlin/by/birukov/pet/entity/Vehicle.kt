package by.birukov.pet.entity

import java.time.LocalDate
import javax.persistence.CascadeType
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.OneToOne
import javax.persistence.Table

/**
 * @author Birukov
 * @since 2021-09-06
 */
@Entity
@Table(name = "vehicles")
class Vehicle(
    var color: String,
    var engineCapacity: Int,
    var yearOfManufacture: LocalDate,
    var weight: Int,
    var hidden: Boolean,

    @Column(name = "reg_number")
    var registerNumber: String,

    @ManyToOne
    @JoinColumn(name = "id_client")
    var owner: Client?,

    @OneToOne(mappedBy = "vehicle", cascade = [CascadeType.ALL], orphanRemoval = true)
    var contract: InsuranceContract?
) : BasicEntity()
