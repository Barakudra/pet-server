package by.birukov.pet.entity

import javax.persistence.CascadeType
import javax.persistence.Entity
import javax.persistence.JoinColumn
import javax.persistence.OneToMany
import javax.persistence.OneToOne
import javax.persistence.Table

/**
 * @author Birukov
 * @since 2021-09-06
 */
@Entity
@Table(name = "insurance_contracts")
class InsuranceContract(

    @OneToOne
    @JoinColumn(name = "id_vehicle")
    var vehicle: Vehicle,

    @OneToMany(cascade = [CascadeType.ALL])
    @JoinColumn(name = "id_contract")
    var insuranceKits: MutableList<InsuranceKit>
) : BasicEntity()