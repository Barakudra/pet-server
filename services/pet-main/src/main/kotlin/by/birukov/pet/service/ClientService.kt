package by.birukov.pet.service

import by.birukov.pet.dto.BasicClientDto
import by.birukov.pet.dto.ClientDetailsDto
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable

/**
 * @author Birukov
 * The class provide possibility to interact with Client entity.
 */
interface ClientService {

    /**
     * The method save new client in database
     * @param clientDetailsDto the filled DTO, which will save
     */
    fun create(clientDetailsDto: ClientDetailsDto): ClientDetailsDto

    /**
     * The method update the client in database
     * @param clientDetailsDto the filled DTO, with new params
     * @return Updated client with new fields
     */
    fun update(clientDetailsDto: ClientDetailsDto): ClientDetailsDto

    /**
     * The method returns sublist of clients which suit a parameters e.t. filtering, size and sort
     *
     * This method searching in database clients considering 3 point:
     * Size - number of clients, which will show at one page.
     * Sort - field and direction of sorting.
     * Filtering - field, that checked for compliance return abjects and skip any other, which do not match the desired criterion.
     * If [filterArgument] doesn't exist - the result does not take it into account.

     * @param filterArgument string filter, according to which the result is selected. Searches for two fields - email and lastName.
     * @param pageable object, which contain info about sorting and size.
     * @return [Page<BasicClientDto>] - special object, which contain list of return entities certain size
     */
    fun search(filterArgument: String, pageable: Pageable): Page<BasicClientDto>

    /**
     * The method returns list of existing entities in database, which are converted to simple view with little count of fields.
     * @return [MutableList(BasicClientDto)]
     */
    fun getAll(): MutableList<BasicClientDto>

    /**
     * The method returns client with detail description.
     *
     * @param [id] index of client in database
     * @return [ClientDetailsDto]
     */
    fun getById(id: Int): ClientDetailsDto

    /** The method finds client by email and returns [ClientDetailsDto] */
    fun findByEmail(email: String): ClientDetailsDto?
}