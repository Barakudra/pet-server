package by.birukov.pet.repository

import by.birukov.pet.entity.AuditTrailEvent
import org.springframework.data.jpa.repository.JpaRepository

interface AuditRepository : JpaRepository<AuditTrailEvent, Int>