package by.birukov.pet.repository

import by.birukov.pet.entity.Vehicle
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

/**
 * @author Birukov
 * @since 2121-09-10
 */
@Repository
interface VehicleRepository : JpaRepository<Vehicle, Int>, VehicleRepositoryCustom {

    /** Searches all [Vehicle] with flag "hidden" false, using Criteria API */
    override fun findAllWithoutHidden(): MutableList<Vehicle>

    /** Method checked existing vehicle id database with register number. */
    fun existsByRegisterNumber(registerNumber: String): Boolean
}