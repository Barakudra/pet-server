package by.birukov.pet.transformer

import by.birukov.pet.dto.BasicClientDto
import by.birukov.pet.dto.ClientDetailsDto
import by.birukov.pet.entity.Client
import org.springframework.stereotype.Component

/**
 * This class provide functions for converting [Client], [ClientDetailsDto] and [BasicClientDto]
 *
 * All fields converted except UUID - this field sets on the service layer.
 */
@Component
class ClientTransformer {

    /** Method convert [Client] to [ClientDetailsDto] */
    fun transform(client: Client): ClientDetailsDto =
        ClientDetailsDto(
            email = client.email,
            password = client.password,
            birthDate = client.birthDate,
            firstName = client.firstName,
            lastName = client.lastName,
            id = client.id
        ).apply {
            createdAt = client.createdAt
            updatedAt = client.updatedAt
        }

    /** Method convert [ClientDetailsDto] to [Client] */
    fun transform(clientDetailsDto: ClientDetailsDto): Client =
        Client(
            email = clientDetailsDto.email,
            password = clientDetailsDto.password,
            birthDate = clientDetailsDto.birthDate,
            firstName = clientDetailsDto.firstName,
            lastName = clientDetailsDto.lastName,
        ).apply {
            id = clientDetailsDto.id
            createdAt = clientDetailsDto.createdAt
            updatedAt = clientDetailsDto.updatedAt
        }

    /** Method convert [Client] to [BasicClientDto] */
    fun transformEntityToBasicClientDto(client: Client): BasicClientDto =
        BasicClientDto(
            id = client.id,
            lastName = client.lastName,
            email = client.email
        )
}