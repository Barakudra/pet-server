package by.birukov.pet.dto

import java.io.Serializable
import javax.validation.constraints.NotBlank
import javax.validation.constraints.PositiveOrZero

/** Array in json from external service. */
data class ExternalYearContentDto(

    @NotBlank
    @PositiveOrZero
    val engineCapacity: Int,

    @NotBlank
    val yearOfManufacture: Int,
) : Serializable {

    companion object {
        private const val serialVersionUID: Long = 8396751436543653L
    }
}
