package by.birukov.pet

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.netflix.eureka.EnableEurekaClient
import org.springframework.cloud.netflix.hystrix.EnableHystrix
import org.springframework.cloud.openfeign.EnableFeignClients

@EnableFeignClients
@SpringBootApplication
@EnableHystrix
@EnableEurekaClient
class PetApplication

fun main(args: Array<String>) {
	runApplication<PetApplication>(*args)
}
