package by.birukov.pet.controller

import by.birukov.pet.dto.ClientDetailsDto
import by.birukov.pet.dto.InsuranceContractDto
import by.birukov.pet.service.InsuranceContractService
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.responses.ApiResponses
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController

/**
 * @author Birukov
 * Provide functional work with [InsuranceContractDto]
 */
@Tag(name = "Contract", description = "The contract API.")

@RestController
@RequestMapping("/contracts")
class InsuranceContractController(
    private val insuranceContractService: InsuranceContractService
) {

    /** Return list of [InsuranceContractDto] */
    @Operation(summary = "Return all of insurance contract.")
    @ApiResponses(ApiResponse(responseCode = "200", description = "Insurance contracts are received."))

    @GetMapping
    fun getAll(): MutableList<InsuranceContractDto> = insuranceContractService.getAll()

    /** Return list of [InsuranceContractDto], which belong to [owner] */
    @Operation(summary = "Return all contracts of client.")
    @ApiResponses(ApiResponse(responseCode = "200", description = "Contracts are received."))

    @PostMapping("/users/{id}")
    fun getContractsOfOwner(@RequestBody owner: ClientDetailsDto): MutableList<InsuranceContractDto> =
        insuranceContractService.getContractsOfOwner(owner)

    /** Create insurance contract from [insuranceContractDto] */
    @Operation(summary = "Create contract.")
    @ApiResponses(ApiResponse(responseCode = "201", description = "Contract was created."))

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    fun create(@RequestBody insuranceContractDto: InsuranceContractDto): InsuranceContractDto =
        insuranceContractService.create(insuranceContractDto)
}