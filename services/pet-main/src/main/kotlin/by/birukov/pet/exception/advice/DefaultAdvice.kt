package by.birukov.pet.exception.advice

import by.birukov.pet.exception.BusinessException
import by.birukov.pet.exception.OldCarException
import by.birukov.pet.exception.TokenException
import org.springframework.http.HttpStatus.CONFLICT
import org.springframework.http.HttpStatus.BAD_REQUEST
import org.springframework.http.HttpStatus.FORBIDDEN
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler

/** Class intercepts exceptions */
@ControllerAdvice
class DefaultAdvice {

    /** Intercept [BusinessException] and send 409 status with message */
    @ExceptionHandler(BusinessException::class)
    fun handleException(exception: BusinessException): ResponseEntity<Any> = ResponseEntity
        .status(CONFLICT)
        .body(exception.message)

    /** Intercept [OldCarException] and send 400 status with message */
    @ExceptionHandler(OldCarException::class)
    fun oldCarException(exception: OldCarException): ResponseEntity<Any> = ResponseEntity
        .status(BAD_REQUEST)
        .body(exception.message)

    /** Intercept [TokenException] and send 403 status if token don't exists or expired */
    @ExceptionHandler(TokenException::class)
    fun tokenException(exception: TokenException): ResponseEntity<Any> = ResponseEntity
        .status(FORBIDDEN)
        .body(exception.message)
}