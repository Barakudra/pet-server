package by.birukov.pet.service.impl

import by.birukov.pet.client.ExternalClient
import by.birukov.pet.service.ExternalService
import org.springframework.stereotype.Service

@Service
class ExternalServiceImpl(private val externalClient: ExternalClient) : ExternalService {

    override fun getYears(): List<Int> = externalClient.getDataFromExternal().content.map { it.yearOfManufacture }
}