package by.birukov.pet.repository.impl

import by.birukov.pet.entity.Client
import by.birukov.pet.repository.ClientRepositoryCustom
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.query.QueryUtils
import javax.persistence.EntityManager
import javax.persistence.PersistenceContext
import javax.persistence.criteria.CriteriaBuilder
import javax.persistence.criteria.CriteriaQuery
import javax.persistence.criteria.Predicate

class ClientRepositoryCustomImpl : ClientRepositoryCustom {

    @PersistenceContext
    private lateinit var entityManager: EntityManager

    override fun search(filterArgument: String, pageable: Pageable): Page<Client> {

        val criteriaBuilder: CriteriaBuilder = entityManager.criteriaBuilder
        val criteriaQuery: CriteriaQuery<Client> = criteriaBuilder.createQuery(Client::class.java)
        val root = criteriaQuery.from(Client::class.java)

        val predicateLastName: Predicate =
            criteriaBuilder.like(criteriaBuilder.lower(root.get("lastName")), "%${filterArgument.lowercase()}%")
        val predicateEmail: Predicate =
            criteriaBuilder.like(criteriaBuilder.lower(root.get("email")), "%${filterArgument.lowercase()}%")
        val finalPredicate: Predicate = criteriaBuilder.or(predicateEmail, predicateLastName)

        criteriaQuery
            .select(root)
            .where(finalPredicate)
            .orderBy(
                QueryUtils.toOrders(pageable.sort, root, criteriaBuilder)
            )

        val query = entityManager.createQuery(criteriaQuery)

        val totalSize: Long = query.resultList.size.toLong()

        query.apply {
            maxResults = pageable.pageSize
            firstResult = pageable.pageNumber * pageable.pageSize
        }
        return PageImpl(query.resultList, pageable, totalSize)
    }
}