package by.birukov.pet.dto

import by.birukov.pet.entity.PartOfVehicle
import java.io.Serializable
import java.time.LocalDate
import java.time.LocalDateTime
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull
import javax.validation.constraints.Positive
import javax.validation.constraints.Size

/**
 * @author Birukov
 * @since 2021-09-06
 */
data class InsuranceKitDto(
    val id: Int = 0,

    @NotNull
    @NotBlank
    val duration: LocalDate,

    @Positive
    @NotBlank
    @Size(min = 1, max = 200)
    val compensationPercent: Int,

    @NotNull
    val damageLevel: String,

    val partsOfVehicle: MutableList<PartOfVehicle>
) : Serializable {

    lateinit var createdAt: LocalDateTime
    lateinit var updatedAt: LocalDateTime

    companion object {
        private const val serialVersionUID: Long = 8527272751086625332L
    }
}
