package by.birukov.pet.repository

import by.birukov.pet.entity.Client
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

/**
 * Class provide work [Client] entity with database.
 *
 * Class implements interfaces, which give functional work with database using hibernate, and allow generate response
 * with pagination
 */
@Repository
interface ClientRepository : JpaRepository<Client, Int>, ClientRepositoryCustom {

    /** Method checked existing client id database with email. */
    fun existsByEmail(email: String): Boolean

    /** Return client with current email */
    fun findByEmail(email: String): Client?
}