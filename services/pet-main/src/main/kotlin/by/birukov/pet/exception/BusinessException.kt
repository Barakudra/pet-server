package by.birukov.pet.exception

class BusinessException(message: String?) : RuntimeException(message)