package by.birukov.pet.entity

import org.springframework.data.annotation.CreatedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener
import java.time.LocalDateTime
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.EntityListeners
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "audit_trail_event")
@EntityListeners(AuditingEntityListener::class)
class AuditTrailEvent() {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Int = 0

    @Column(name = "object_type")
    lateinit var objectType: String

    @Column(name = "object_id")
    var objectId: Int = 0

    @Column(name = "action_type")
    lateinit var actionType: String

    @CreatedDate
    lateinit var date: LocalDateTime

    constructor(
        objectType: String,
        objectId: Int,
        actionType: String,
    ) : this() {

        this.objectType = objectType
        this.objectId = objectId
        this.actionType = actionType
    }
}