package by.birukov.pet.service

import by.birukov.pet.dto.InsuranceKitDto

interface InsuranceKitService {

    /**
     * The method create [InsuranceKitDto].
     *
     * The method create kit as part of creating contract.
     * @param [insuranceKitDto] - the filled dto.
     */
    fun create(insuranceKitDto: InsuranceKitDto): InsuranceKitDto

    /**
     * The method return list of stored kits in database
     *
     * @return [MutableList(InsuranceKitDto)]
     */
    fun getAll(): MutableList<InsuranceKitDto>
}