package by.birukov.pet.aspect

import by.birukov.pet.dto.VehicleDetailsDto
import by.birukov.pet.entity.AuditTrailEvent
import by.birukov.pet.repository.AuditRepository
import org.aspectj.lang.JoinPoint
import org.aspectj.lang.annotation.AfterReturning
import org.aspectj.lang.annotation.Aspect
import org.aspectj.lang.annotation.Pointcut
import org.springframework.stereotype.Component

@Aspect
@Component
class VehicleAuditAspect(private val auditRepository: AuditRepository) {

    @Pointcut("execution(* by.birukov.pet.service.VehicleService.delete(..))")
    fun deleteVehicle() = Unit

    @Pointcut("execution(* by.birukov.pet.service.VehicleService.create(..))")
    fun createVehicle() = Unit

    @AfterReturning(value = "createVehicle()", returning = "createdVehicle")
    fun afterCreateVehicle(joinPoint: JoinPoint, createdVehicle: VehicleDetailsDto) {
        val auditTrailEvent = AuditTrailEvent(
            TYPE_VEHICLE,
            createdVehicle.id,
            ACTION_CREATE
        )
        auditRepository.save(auditTrailEvent)
    }

    @AfterReturning(value = "deleteVehicle()", returning = "deletedVehicle")
    fun afterDeleteVehicle(joinPoint: JoinPoint, deletedVehicle: VehicleDetailsDto) {
        val auditTrailEvent = AuditTrailEvent(
            TYPE_VEHICLE,
            deletedVehicle.id,
            ACTION_DELETE
        )
        auditRepository.save(auditTrailEvent)
    }

    companion object {
        private const val TYPE_VEHICLE: String = "VEHICLE"
        private const val ACTION_DELETE: String = "DELETE"
        private const val ACTION_CREATE: String = "CREATE"
    }
}