package by.birukov.pet.transformer

import by.birukov.pet.dto.BasicVehicleDto
import by.birukov.pet.dto.VehicleDetailsDto
import by.birukov.pet.entity.Vehicle
import org.springframework.stereotype.Component

/**
 * This class provide functions for converting [Vehicle], [VehicleDetailsDto] and [BasicVehicleDto]
 *
 * All fields converted except UUID - this field sets on the service layer.
 */
@Component
class VehicleTransformer(
    private val clientTransformer: ClientTransformer,
) {

    /**
     * Method convert [Vehicle] to [VehicleDetailsDto]
     *
     * @param [vehicle] - vehicle entity from database.
     * @return [VehicleDetailsDto] - Dto with details
     */
    fun transform(vehicle: Vehicle): VehicleDetailsDto =
        VehicleDetailsDto(
            id = vehicle.id,
            color = vehicle.color,
            engineCapacity = vehicle.engineCapacity,
            yearOfManufacture = vehicle.yearOfManufacture,
            weight = vehicle.weight,
            registerNumber = vehicle.registerNumber,
            hidden = vehicle.hidden
        ).apply {
            owner = vehicle.owner?.let { clientTransformer.transform(it) }
            updatedAt = vehicle.updatedAt
            createdAt = vehicle.createdAt
            contract = vehicle.contract
            isContractExist = contract != null
        }

    /**
     * Method convert [VehicleDetailsDto] to [Vehicle]
     *
     * @param [vehicleDetailsDto] - Dto with filled fields
     * @return [Vehicle] - Entity of vehicle
     */
    fun transform(vehicleDetailsDto: VehicleDetailsDto): Vehicle =
        Vehicle(
            owner = vehicleDetailsDto.owner?.let { clientTransformer.transform(it) },
            color = vehicleDetailsDto.color,
            engineCapacity = vehicleDetailsDto.engineCapacity,
            yearOfManufacture = vehicleDetailsDto.yearOfManufacture,
            weight = vehicleDetailsDto.weight,
            hidden = vehicleDetailsDto.hidden,
            registerNumber = vehicleDetailsDto.registerNumber,
            contract = vehicleDetailsDto.contract,
        ).apply {
            id = vehicleDetailsDto.id
            updatedAt = vehicleDetailsDto.updatedAt
            createdAt = vehicleDetailsDto.createdAt
        }

    /**
     * Method convert [Vehicle] to [BasicVehicleDto]
     *
     * @param [vehicle] - vehicle entity from database.
     * @return [BasicVehicleDto] - dto with basic info (id, register number)
     */
    fun transformEntityToBasicVehicleDto(vehicle: Vehicle): BasicVehicleDto =
        BasicVehicleDto(
            id = vehicle.id,
            registerNumber = vehicle.registerNumber,
            isContractExist = vehicle.contract != null
        )
}