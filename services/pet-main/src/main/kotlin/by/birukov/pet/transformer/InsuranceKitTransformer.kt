package by.birukov.pet.transformer

import by.birukov.pet.dto.InsuranceKitDto
import by.birukov.pet.entity.InsuranceKit
import org.springframework.stereotype.Component

/**
 * This class provide functions for converting [InsuranceKitDto] and [InsuranceKit] to each other
 *
 * All fields converted except UUID - this field sets on the service layer.
 */
@Component
class InsuranceKitTransformer {

    /**
     * Method convert [InsuranceKit] to [InsuranceKitDto]
     *
     * @param [insuranceKit] - entity from database.
     * @return [InsuranceKitDto] - Dto
     */
    fun transform(insuranceKit: InsuranceKit): InsuranceKitDto =
        InsuranceKitDto(
            id = insuranceKit.id,
            duration = insuranceKit.duration,
            compensationPercent = insuranceKit.compensationPercent,
            damageLevel = insuranceKit.damageLevel,
            partsOfVehicle = insuranceKit.partOfVehicle
        ).apply {
            updatedAt = insuranceKit.updatedAt
            createdAt = insuranceKit.createdAt
        }

    /**
     * Method convert [InsuranceKitDto] to [InsuranceKit]
     *
     * @param [insuranceKitDto] - Dto, hat will converted to entity
     * @return [InsuranceKit] - Entity of insurance kit
     */
    fun transform(insuranceKitDto: InsuranceKitDto): InsuranceKit =
        InsuranceKit(
            duration = insuranceKitDto.duration,
            compensationPercent = insuranceKitDto.compensationPercent,
            damageLevel = insuranceKitDto.damageLevel,
            partOfVehicle = insuranceKitDto.partsOfVehicle
        ).apply {
            id = insuranceKitDto.id
            updatedAt = insuranceKitDto.updatedAt
            createdAt = insuranceKitDto.createdAt
        }
}