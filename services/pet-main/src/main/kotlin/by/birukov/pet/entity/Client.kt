package by.birukov.pet.entity

import java.time.LocalDate
import javax.persistence.Entity
import javax.persistence.Table

/**
 * @author Birukov
 * @since 2021-09-06
 */
@Entity
@Table(name = "clients")
class Client(
    var email: String,
    var password: String,
    var birthDate: LocalDate,
    var firstName: String,
    var lastName: String
) : BasicEntity()
