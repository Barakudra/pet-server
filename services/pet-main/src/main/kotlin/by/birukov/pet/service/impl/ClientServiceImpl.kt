package by.birukov.pet.service.impl

import by.birukov.pet.dto.BasicClientDto
import by.birukov.pet.dto.ClientDetailsDto
import by.birukov.pet.entity.Client
import by.birukov.pet.exception.BusinessException
import by.birukov.pet.repository.ClientRepository
import by.birukov.pet.service.ClientService
import by.birukov.pet.transformer.ClientTransformer
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service
import java.util.UUID
import javax.persistence.EntityNotFoundException

@Service
class ClientServiceImpl(
    private val clientRepository: ClientRepository,
    private val clientTransformer: ClientTransformer,
    private val bCrypt: PasswordEncoder
) : ClientService {

    override fun search(filterArgument: String, pageable: Pageable): Page<BasicClientDto> =
        clientRepository.search(filterArgument, pageable)
            .map { clientTransformer.transformEntityToBasicClientDto(it) }

    override fun getAll(): MutableList<BasicClientDto> = clientRepository.findAll()
        .map { clientTransformer.transformEntityToBasicClientDto(it) }.toMutableList()

    override fun getById(id: Int): ClientDetailsDto = clientTransformer.transform(clientRepository.findById(id).get())

    override fun create(clientDetailsDto: ClientDetailsDto): ClientDetailsDto =
        clientTransformer.transform(clientDetailsDto).apply {
            uuid = UUID.randomUUID().toString()
            password = bCrypt.encode(password)
        }.let {
            it.validate()
            clientTransformer.transform(clientRepository.save(it))
        }

    override fun update(clientDetailsDto: ClientDetailsDto): ClientDetailsDto =
        clientRepository.findById(clientDetailsDto.id).orElseThrow {
            EntityNotFoundException(NOT_FOUND_EXCEPTION)
        }.apply {
            firstName = clientDetailsDto.firstName
            lastName = clientDetailsDto.lastName
            birthDate = clientDetailsDto.birthDate
        }.let { clientTransformer.transform(clientRepository.save(it)) }

    override fun findByEmail(email: String): ClientDetailsDto? =
        clientRepository.findByEmail(email)?.let { clientTransformer.transform(it) }

    private fun Client.validate(): Unit {
        if (this.isEmailExist(email)) {
            throw BusinessException(EMAIL_EXIST_EXCEPTION)
        }
    }

    private fun Client.isEmailExist(email: String): Boolean = clientRepository.existsByEmail(email)

    companion object {
        private const val NOT_FOUND_EXCEPTION: String = "Client with this id was not found"
        private const val WRONG_EMAIL_OR_PASSWORD: String = "Wrong email or password"
        private const val EMAIL_EXIST_EXCEPTION: String = "emailNotUnique"
    }
}


