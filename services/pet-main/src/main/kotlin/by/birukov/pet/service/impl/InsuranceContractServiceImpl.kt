package by.birukov.pet.service.impl

import by.birukov.pet.dto.ClientDetailsDto
import by.birukov.pet.dto.InsuranceContractDto
import by.birukov.pet.repository.InsuranceContractRepository
import by.birukov.pet.service.InsuranceContractService
import by.birukov.pet.transformer.ClientTransformer
import by.birukov.pet.transformer.InsuranceContractTransformer
import org.springframework.stereotype.Service
import java.util.UUID

@Service
class InsuranceContractServiceImpl(
    private val insuranceContractRepository: InsuranceContractRepository,
    private val insuranceContractTransformer: InsuranceContractTransformer,
    private val clientTransformer: ClientTransformer
) : InsuranceContractService {

    override fun getAll(): MutableList<InsuranceContractDto> =
        insuranceContractRepository.findAll().map {
            insuranceContractTransformer.transform(it)
        }.toMutableList()

    override fun create(insuranceContractDto: InsuranceContractDto): InsuranceContractDto =
        insuranceContractTransformer.transform(insuranceContractDto).apply {
            uuid = UUID.randomUUID().toString()
        }.let {
            it.insuranceKits.map {
                it.uuid = UUID.randomUUID().toString()
            }
            insuranceContractTransformer.transform(insuranceContractRepository.save(it))
        }

    override fun getContractsOfOwner(owner: ClientDetailsDto): MutableList<InsuranceContractDto> =
        insuranceContractRepository.findAllByVehicleOwner(clientTransformer.transform(owner)).map {
            insuranceContractTransformer.transform(it)
        }.toMutableList()
}