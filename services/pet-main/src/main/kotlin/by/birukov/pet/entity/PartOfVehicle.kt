package by.birukov.pet.entity

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table

/**
 * @author Birukov
 * @since 2021-09-06
 */
@Entity
@Table(name = "part_of_vehicles")
class PartOfVehicle(

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Int,

    @Column(name = "vehicle_part")
    var vehiclePart: String,
)
