package by.birukov.pet.repository

import by.birukov.pet.entity.Vehicle
import org.springframework.stereotype.Repository

/** Class provide working with [Vehicle] at Criteria API */
@Repository
interface VehicleRepositoryCustom {
    fun findAllWithoutHidden(): MutableList<Vehicle>
}