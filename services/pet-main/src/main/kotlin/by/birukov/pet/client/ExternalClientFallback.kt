package by.birukov.pet.client

import by.birukov.pet.dto.ExternalYearDto
import org.springframework.stereotype.Component

/** Class provide functionality when [ExternalClient] fallen */
@Component
class ExternalClientFallback : ExternalClient {
    override fun getDataFromExternal(): ExternalYearDto = ExternalYearDto(
        message = "",
        amount = 0,
        type = "",
        content = emptyList()
    )
}