package by.birukov.pet.entity

import java.time.LocalDate
import javax.persistence.CascadeType
import javax.persistence.Entity
import javax.persistence.EnumType
import javax.persistence.Enumerated
import javax.persistence.JoinColumn
import javax.persistence.JoinTable
import javax.persistence.ManyToMany
import javax.persistence.Table

/**
 * @author Birukov
 * @since 2021-09-06
 */
@Entity
@Table(name = "insurance_kits")
class InsuranceKit(
    var duration: LocalDate,
    var compensationPercent: Int,
    var damageLevel: String,

    @ManyToMany(cascade = [CascadeType.ALL])
    @JoinTable(
        name = "kits_vehicles",
        joinColumns = [JoinColumn(name = "id_insurance_kit")],
        inverseJoinColumns = [JoinColumn(name = "id_part")]
    )

    @Enumerated(EnumType.STRING)
    var partOfVehicle: MutableList<PartOfVehicle>
) : BasicEntity()
