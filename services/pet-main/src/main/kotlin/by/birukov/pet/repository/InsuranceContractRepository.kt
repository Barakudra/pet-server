package by.birukov.pet.repository

import by.birukov.pet.entity.Client
import by.birukov.pet.entity.InsuranceContract
import org.springframework.data.jpa.repository.JpaRepository

/**
 * Class provide work [InsuranceContract] entity with database.
 *
 * Class implements interfaces, which give functional work with database using hibernate
 */
interface InsuranceContractRepository : JpaRepository<InsuranceContract, Int> {

    /** The method returns list of contract of owner */
    fun findAllByVehicleOwner(owner: Client): MutableList<InsuranceContract>
}