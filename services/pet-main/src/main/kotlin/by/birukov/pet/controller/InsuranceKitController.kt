package by.birukov.pet.controller

import by.birukov.pet.dto.InsuranceKitDto
import by.birukov.pet.service.InsuranceKitService
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.responses.ApiResponses
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController

/**
 * @author Birukov
 * Provide functional work with [InsuranceKitDto]
 */
@Tag(name = "InsuranceKit", description = "The insurance kit API.")

@RestController
@RequestMapping("/complexes")
class InsuranceKitController(private val insuranceKitService: InsuranceKitService) {

    /** Return list of [InsuranceKitDto] */
    @Operation(summary = "Get all insurance kits.")
    @ApiResponses(ApiResponse(responseCode = "200", description = "Insurance kits was received"))

    @GetMapping
    fun getAll(): MutableList<InsuranceKitDto> = insuranceKitService.getAll()

    /** Create insurance kit from [insuranceKitDto] */
    @Operation(summary = "Get all insurance kits.")
    @ApiResponses(ApiResponse(responseCode = "201", description = "Insurance kits was created."))

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    fun create(@RequestBody insuranceKitDto: InsuranceKitDto): InsuranceKitDto =
        insuranceKitService.create(insuranceKitDto)
}