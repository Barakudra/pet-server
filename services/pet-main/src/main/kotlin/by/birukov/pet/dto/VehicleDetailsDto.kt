package by.birukov.pet.dto

import by.birukov.pet.entity.InsuranceContract
import com.fasterxml.jackson.annotation.JsonIgnore
import java.io.Serializable
import java.time.LocalDate
import java.time.LocalDateTime
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull
import javax.validation.constraints.PastOrPresent
import javax.validation.constraints.Positive
import javax.validation.constraints.Size

/**
 * @author Birukov
 * @since 2021-09-06
 */
data class VehicleDetailsDto(
    val id: Int,

    @NotNull
    val color: String,

    @NotBlank
    @Positive
    val engineCapacity: Int,

    @NotBlank
    @NotNull
    @PastOrPresent
    val yearOfManufacture: LocalDate,

    @NotBlank
    @NotNull
    @Size(min = 4, max = 10)
    val registerNumber: String,

    @Positive
    @NotNull
    @NotBlank
    val weight: Int,
    
    val hidden: Boolean
) : Serializable {

    lateinit var createdAt: LocalDateTime
    lateinit var updatedAt: LocalDateTime
    var owner: ClientDetailsDto? = null
    var isContractExist: Boolean? = null

    @JsonIgnore
    var contract: InsuranceContract? = null

    companion object {
        private const val serialVersionUID: Long = 6137430551986625331L
    }
}
