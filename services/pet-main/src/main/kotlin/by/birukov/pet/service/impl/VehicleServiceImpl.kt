package by.birukov.pet.service.impl

import by.birukov.pet.dto.BasicVehicleDto
import by.birukov.pet.dto.VehicleDetailsDto
import by.birukov.pet.entity.Vehicle
import by.birukov.pet.exception.BusinessException
import by.birukov.pet.repository.VehicleRepository
import by.birukov.pet.service.VehicleService
import by.birukov.pet.transformer.VehicleTransformer
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.stereotype.Service
import java.util.UUID
import javax.persistence.EntityNotFoundException

@Service
@ConditionalOnProperty(
    name = ["beans.candidates.vehicle.service"],
    havingValue = "default",
    matchIfMissing = true
)
class VehicleServiceImpl(
    private val vehicleRepository: VehicleRepository,
    private val vehicleTransformer: VehicleTransformer
) : VehicleService {

    override fun getById(id: Int): VehicleDetailsDto = vehicleTransformer.transform(vehicleRepository.getById(id))

    override fun findAll(): List<BasicVehicleDto> = vehicleRepository.findAll().map {
        vehicleTransformer.transformEntityToBasicVehicleDto(it)
    }

    override fun create(vehicleDetailsDto: VehicleDetailsDto): VehicleDetailsDto =
        vehicleTransformer.transform(vehicleDetailsDto).apply { uuid = UUID.randomUUID().toString() }
            .let {
                it.validate()
                vehicleTransformer.transform(vehicleRepository.save(it))
            }

    override fun delete(id: Int): VehicleDetailsDto =
        vehicleTransformer.transform(vehicleRepository.findById(id).orElseThrow {
            EntityNotFoundException(NOT_FOUND_EXCEPTION)
        }.apply { hidden = true }.let { vehicleRepository.save(it) })

    override fun findAllUndeleted(): List<BasicVehicleDto> = vehicleRepository.findAllWithoutHidden().map {
        vehicleTransformer.transformEntityToBasicVehicleDto(it)
    }

    private fun Vehicle.isRegNumberExists(): Boolean =
        vehicleRepository.existsByRegisterNumber(this.registerNumber)

    private fun Vehicle.validate() {
        if (this.isRegNumberExists()) {
            throw BusinessException(REGISTER_NUMBER_EXIST)
        }
    }

    companion object {
        private const val NOT_FOUND_EXCEPTION: String = "Vehicle with this id was not found"
        private const val REGISTER_NUMBER_EXIST: String = "regNumberExist"
    }
}