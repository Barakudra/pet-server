package by.birukov.pet.service.impl

import by.birukov.pet.dto.BasicVehicleDto
import by.birukov.pet.dto.VehicleDetailsDto
import by.birukov.pet.exception.BusinessException
import by.birukov.pet.service.VehicleService
import by.birukov.pet.transformer.VehicleTransformer
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.stereotype.Service
import java.time.LocalDate
import java.time.LocalDateTime

/** Class provide functional for fake work with vehicles. It doesn't work with database. */

@Service
@ConditionalOnProperty(
    name = ["beans.candidates.vehicle.service"],
    havingValue = "stub"
)
class StubVehicleServiceImpl(private val vehicleTransformer: VehicleTransformer) : VehicleService {

    private var vehicles = mutableListOf<VehicleDetailsDto>(
        VehicleDetailsDto(
            id = 1,
            color = "stub color",
            engineCapacity = 0,
            yearOfManufacture = LocalDate.now(),
            registerNumber = "stub number",
            weight = 1,
            hidden = false
        ).apply {
            createdAt = LocalDateTime.now()
            updatedAt = LocalDateTime.now()
        }
    )

    override fun getById(id: Int): VehicleDetailsDto {
        vehicles.forEach {
            if (it.id == id) {
                return it
            }
        }
        throw BusinessException(NOT_FOUND_EXCEPTION)
    }

    override fun findAll(): List<BasicVehicleDto> =
        vehicles.map { vehicleTransformer.transformEntityToBasicVehicleDto(vehicleTransformer.transform(it)) }

    override fun create(vehicleDetailsDto: VehicleDetailsDto): VehicleDetailsDto {
        vehicleDetailsDto.isRegNumberExists()
        vehicles.add(vehicleDetailsDto)
        return vehicleDetailsDto
    }

    override fun delete(id: Int): VehicleDetailsDto {
        vehicles.forEachIndexed { position, vehicle ->
            if (vehicle.id == id) {
                vehicles.removeAt(position)
                return vehicle
            }
        }
        throw BusinessException(NOT_FOUND_EXCEPTION)
    }

    override fun findAllUndeleted(): List<BasicVehicleDto> = findAll()

    private fun VehicleDetailsDto.isRegNumberExists(): Boolean {
        vehicles.forEach {
            if (it.registerNumber == this.registerNumber) {
                throw BusinessException(REGISTER_NUMBER_EXIST)
            }
        }
        return false
    }

    companion object {
        private const val NOT_FOUND_EXCEPTION: String = "Vehicle with this id was not found"
        private const val REGISTER_NUMBER_EXIST: String = "regNumberExist"
    }
}