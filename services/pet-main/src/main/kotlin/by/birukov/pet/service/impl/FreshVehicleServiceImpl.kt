package by.birukov.pet.service.impl

import by.birukov.pet.dto.BasicVehicleDto
import by.birukov.pet.dto.VehicleDetailsDto
import by.birukov.pet.exception.OldCarException
import by.birukov.pet.repository.VehicleRepository
import by.birukov.pet.service.VehicleService
import by.birukov.pet.transformer.VehicleTransformer
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.context.annotation.Primary
import org.springframework.stereotype.Service
import java.time.LocalDate

/** Class provide functional for work only with vehicles, which was produced less than 5 years ago */

@Service
@Primary
@ConditionalOnProperty(
    name = ["beans.candidates.vehicle.service"],
    havingValue = "fresh"
)
class FreshVehicleServiceImpl(
    private val vehicleTransformer: VehicleTransformer,
    private val vehicleRepository: VehicleRepository,
) : VehicleService {

    var vehicleServiceImpl: VehicleServiceImpl = VehicleServiceImpl(vehicleRepository, vehicleTransformer)

    override fun getById(id: Int): VehicleDetailsDto =
        vehicleServiceImpl.getById(id).apply { checkFresh() }

    override fun findAll(): List<BasicVehicleDto> = vehicleServiceImpl.findAllUndeleted()

    override fun create(vehicleDetailsDto: VehicleDetailsDto): VehicleDetailsDto =
        vehicleDetailsDto.checkFresh().run {
            vehicleServiceImpl.create(vehicleDetailsDto)
        }

    override fun delete(id: Int): VehicleDetailsDto =
        getById(id).checkFresh().run {
            vehicleServiceImpl.delete(id)
        }

    override fun findAllUndeleted(): List<BasicVehicleDto> = vehicleServiceImpl.findAllUndeleted()

    private fun VehicleDetailsDto.checkFresh() {
        if (LocalDate.now().year - this.yearOfManufacture.year > 5) {
            throw OldCarException(VERY_OLD_CAR_EXCEPTION)
        }
    }

    companion object {
        private const val VERY_OLD_CAR_EXCEPTION: String = "veryOldCar"
    }
}