package by.birukov.pet.transformer

import by.birukov.pet.dto.InsuranceContractDto
import by.birukov.pet.entity.InsuranceContract
import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Component

/**
 * This class provide functions for converting [InsuranceContract] and [InsuranceContractDto] to each other
 *
 * All fields converted except UUID - this field sets on the service layer.
 */
@Component
class InsuranceContractTransformer(
    @Lazy private val vehicleTransformer: VehicleTransformer,
    @Lazy private val insuranceKitTransformer: InsuranceKitTransformer
) {

    /**
     * Method convert [InsuranceContract] to [InsuranceContractDto]
     *
     * @param [insuranceContract] - entity from database.
     * @return [InsuranceContractDto] - Dto
     */
    fun transform(insuranceContract: InsuranceContract): InsuranceContractDto =
        InsuranceContractDto(
            id = insuranceContract.id,
            insuranceKits = insuranceContract.insuranceKits.map { insuranceKitTransformer.transform(it) }
                .toMutableList()
        ).apply {
            vehicle = vehicleTransformer.transform(insuranceContract.vehicle)
            updatedAt = insuranceContract.updatedAt
            createdAt = insuranceContract.createdAt
        }

    /**
     * Method convert [InsuranceContractDto] to [InsuranceContract]
     *
     * @param [insuranceContractDto] - Dto, hat will converted to entity
     * @return [InsuranceContract] - Entity of Insurance Contract
     */
    fun transform(insuranceContractDto: InsuranceContractDto): InsuranceContract =
        InsuranceContract(
            vehicle = vehicleTransformer.transform(insuranceContractDto.vehicle),
            insuranceKits = insuranceContractDto.insuranceKits.map { insuranceKitTransformer.transform(it) }
                .toMutableList()
        ).apply {
            id = insuranceContractDto.id
            createdAt = insuranceContractDto.createdAt
            updatedAt = insuranceContractDto.updatedAt
        }
}