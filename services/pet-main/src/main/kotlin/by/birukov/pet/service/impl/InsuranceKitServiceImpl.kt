package by.birukov.pet.service.impl

import by.birukov.pet.dto.InsuranceKitDto
import by.birukov.pet.repository.InsuranceKitRepository
import by.birukov.pet.service.InsuranceKitService
import by.birukov.pet.transformer.InsuranceKitTransformer
import org.springframework.stereotype.Service
import java.util.UUID

@Service
class InsuranceKitServiceImpl(
    private val insuranceKitRepository: InsuranceKitRepository,
    private val insuranceKitTransformer: InsuranceKitTransformer
) : InsuranceKitService {

    override fun create(insuranceKitDto: InsuranceKitDto): InsuranceKitDto =
        insuranceKitTransformer.transform(insuranceKitDto).apply {
            uuid = UUID.randomUUID().toString()
        }.let { insuranceKitTransformer.transform(insuranceKitRepository.save(it)) }

    override fun getAll(): MutableList<InsuranceKitDto> = insuranceKitRepository.findAll().map {
        insuranceKitTransformer.transform(it)
    }.toMutableList()
}