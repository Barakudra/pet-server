package by.birukov.pet.aspect

import by.birukov.pet.dto.InsuranceContractDto
import by.birukov.pet.entity.AuditTrailEvent
import by.birukov.pet.repository.AuditRepository
import org.aspectj.lang.JoinPoint
import org.aspectj.lang.annotation.AfterReturning
import org.aspectj.lang.annotation.Aspect
import org.aspectj.lang.annotation.Pointcut
import org.springframework.stereotype.Component

@Aspect
@Component
class ContractAspect(private val auditRepository: AuditRepository) {

    @Pointcut("execution(* by.birukov.pet.service.InsuranceContractService.create(..))")
    fun createContract() = Unit

    @AfterReturning(value = "createContract()", returning = "insuranceContractDto")
    fun afterCreateContract(joinPoint: JoinPoint, insuranceContractDto: InsuranceContractDto) {
        val auditTrailEvent = AuditTrailEvent(
            TYPE_CONTRACT,
            insuranceContractDto.id,
            ACTION_CREATE
        )
        auditRepository.save(auditTrailEvent)
    }

    companion object {
        private const val TYPE_CONTRACT: String = "CONTRACT"
        private const val ACTION_CREATE: String = "CREATE"
    }
}