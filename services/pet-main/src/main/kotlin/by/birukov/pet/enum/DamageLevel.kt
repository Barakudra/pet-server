package by.birukov.pet.enum

/** Available damage level of vehicle. */
enum class DamageLevel {
    LOW, MEDIUM, HIGH
}