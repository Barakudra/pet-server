package by.birukov.pet.aspect

import by.birukov.pet.dto.InsuranceKitDto
import by.birukov.pet.entity.AuditTrailEvent
import by.birukov.pet.repository.AuditRepository
import org.aspectj.lang.JoinPoint
import org.aspectj.lang.annotation.AfterReturning
import org.aspectj.lang.annotation.Aspect
import org.aspectj.lang.annotation.Pointcut
import org.springframework.stereotype.Component

@Aspect
@Component
class InsuranceKitAuditAspect(private val auditRepository: AuditRepository) {

    @Pointcut("execution(* by.birukov.pet.service.InsuranceKitService.create(..))")
    fun createInsuranceKit() = Unit

    @AfterReturning(value = "createInsuranceKit()", returning = "insuranceKitDto")
    fun afterCreateInsuranceKit(joinPoint: JoinPoint, insuranceKitDto: InsuranceKitDto) {
        val auditTrailEvent = AuditTrailEvent(
            TYPE_INSURANCE_KIT,
            insuranceKitDto.id,
            ACTION_CREATE
        )
        auditRepository.save(auditTrailEvent)
    }

    companion object {
        private const val TYPE_INSURANCE_KIT: String = "INSURANCE_KIT"
        private const val ACTION_CREATE: String = "CREATE"
    }
}