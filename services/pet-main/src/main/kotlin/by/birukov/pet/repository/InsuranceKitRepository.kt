package by.birukov.pet.repository

import by.birukov.pet.entity.InsuranceKit
import org.springframework.data.jpa.repository.JpaRepository

/**
 * @author Birukov
 * @since 2021-09-06
 */
interface InsuranceKitRepository : JpaRepository<InsuranceKit, Int>