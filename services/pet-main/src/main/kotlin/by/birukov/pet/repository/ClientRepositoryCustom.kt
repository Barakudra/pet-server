package by.birukov.pet.repository

import by.birukov.pet.entity.Client
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Repository

/** Class provide working with [Page(Client)] at Criteria API */
@Repository
interface ClientRepositoryCustom {
    fun search(filterArgument: String, pageable: Pageable): Page<Client>
}