package by.birukov.pet.dto

import java.io.Serializable
import javax.validation.constraints.NotBlank
import javax.validation.constraints.Size

/**
 * @author Birukov
 * @since 2021-09-06
 */
data class BasicClientDto(
    val id: Int = 0,

    @NotBlank
    @Size(min = 3, max = 200)
    val lastName: String,

    @NotBlank
    @Size(min = 3, max = 200)
    var email: String
) : Serializable {

    companion object {
        private const val serialVersionUID: Long = 7271400551086637515L
    }
}
