package by.birukov.pet.exception

class OldCarException(message: String) : RuntimeException(message)