package by.birukov.pet.client

import by.birukov.pet.dto.ExternalYearDto
import org.springframework.cloud.openfeign.FeignClient
import org.springframework.web.bind.annotation.GetMapping

/** Communication with external API. */
@FeignClient(
    value = "foreign",
    url = "https://demo-vehicles-project-api.s3.us-east-2.amazonaws.com/vehicleSample.json",
    fallback = ExternalClientFallback::class
)
interface ExternalClient {

    /** Return [ExternalYearDto] from external API. */
    @GetMapping
    fun getDataFromExternal(): ExternalYearDto
}