package by.birukov.pet.controller

import by.birukov.pet.dto.BasicClientDto
import by.birukov.pet.dto.ClientDetailsDto
import by.birukov.pet.service.ClientService
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.responses.ApiResponses
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.web.PageableDefault
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController

/**
 * @author Birukov
 * Controller that processing requests with users
 */
@Tag(name = "User", description = "The User API")

@RestController
@RequestMapping("/users")
class ClientController(private val clientService: ClientService) {

    /** Return page of clients */
    @Operation(summary = "Get pageable of clients")
    @ApiResponses(ApiResponse(responseCode = "200", description = "Pageable with clients are received"))

    @GetMapping
    fun getAll(
        @PageableDefault pageable: Pageable,
        @RequestParam("filter") filterArgument: String
    ): Page<BasicClientDto> = clientService.search(filterArgument, pageable)

    /** Return list of all clients */
    @Operation(summary = "Get all clients")
    @ApiResponses(ApiResponse(responseCode = "200", description = "Return list of all clients with basic info"))

    @GetMapping("/all")
    fun getAll(): MutableList<BasicClientDto> = clientService.getAll()

    /** Return [ClientDetailsDto] by [id] */
    @Operation(summary = "Return detail about one client")
    @ApiResponses(
        ApiResponse(responseCode = "200", description = "Return client by id"),
        ApiResponse(responseCode = "500", description = "There is no such user"),
    )

    @GetMapping("/{id}")
    fun getById(@PathVariable id: Int): ClientDetailsDto = clientService.getById(id)

    /** Create client from [ClientDetailsDto] */
    @Operation(summary = "Create client")
    @ApiResponses(
        ApiResponse(responseCode = "201", description = "Client was created"),
        ApiResponse(responseCode = "409", description = "Email not unique"),
    )

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    fun create(@RequestBody clientDetailsDto: ClientDetailsDto): ClientDetailsDto =
        clientService.create(clientDetailsDto)

    /** Update client with [id] and sets fields as [clientDetailsDto] */
    @Operation(summary = "Update client")
    @ApiResponses(ApiResponse(responseCode = "200", description = "Resource successfully updated"))

    @PutMapping("/{id}")
    fun update(
        @PathVariable id: Int,
        @RequestBody clientDetailsDto: ClientDetailsDto
    ): ClientDetailsDto = clientService.update(clientDetailsDto)

    /** Return [ClientDetailsDto] if exist with provided email */
    @GetMapping("/authentication")
    fun auth(@RequestParam email: String): ClientDetailsDto? = clientService.findByEmail(email)
}