package by.birukov.pet.service

import org.springframework.stereotype.Service

/** Provide communicate with external client */
@Service
interface ExternalService {

    /** The method returns list of year of manufacture */
    fun getYears(): List<Int>
}