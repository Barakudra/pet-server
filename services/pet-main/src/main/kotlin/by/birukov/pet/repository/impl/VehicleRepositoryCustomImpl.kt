package by.birukov.pet.repository.impl

import by.birukov.pet.entity.Vehicle
import by.birukov.pet.repository.VehicleRepositoryCustom
import javax.persistence.EntityManager
import javax.persistence.PersistenceContext
import javax.persistence.criteria.CriteriaBuilder
import javax.persistence.criteria.CriteriaQuery

class VehicleRepositoryCustomImpl : VehicleRepositoryCustom{
    @PersistenceContext
    private lateinit var entityManager: EntityManager
    override fun findAllWithoutHidden(): MutableList<Vehicle> {

        val criteriaBuilder: CriteriaBuilder = entityManager.criteriaBuilder
        val criteriaQuery: CriteriaQuery<Vehicle> = criteriaBuilder.createQuery(Vehicle::class.java)

        val root = criteriaQuery.from(Vehicle::class.java)
        criteriaQuery
            .select(root)
            .where(
                criteriaBuilder.equal(root.get<Boolean>("hidden"), false)
            )
        val query = entityManager.createQuery(criteriaQuery)

        return query.resultList
    }

}