package by.birukov.pet.dto

import java.io.Serializable
import java.time.LocalDateTime
import javax.validation.constraints.NotBlank

/**
 * @author Birukov
 * @since 2021-09-06
 */
data class InsuranceContractDto(
    val id: Int = 0,

    @NotBlank
    val insuranceKits: MutableList<InsuranceKitDto>
) : Serializable {

    lateinit var vehicle: VehicleDetailsDto
    lateinit var createdAt: LocalDateTime
    lateinit var updatedAt: LocalDateTime

    companion object {
        private const val serialVersionUID: Long = 8527272751086625332L
    }
}
