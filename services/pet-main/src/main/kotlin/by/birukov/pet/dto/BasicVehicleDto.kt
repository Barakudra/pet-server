package by.birukov.pet.dto

import java.io.Serializable
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

/**
 * @author Birukov
 * @since 2021-09-06
 */
data class BasicVehicleDto(
    val id: Int = 0,

    @NotBlank
    @NotNull
    @Size(min = 4, max = 10)
    val registerNumber: String,

    var isContractExist: Boolean? = null
) : Serializable {

    companion object {
        private const val serialVersionUID: Long = 9133400551086625375L
    }
}
