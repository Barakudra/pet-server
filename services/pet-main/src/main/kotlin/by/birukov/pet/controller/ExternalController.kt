package by.birukov.pet.controller

import by.birukov.pet.service.ExternalService
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.responses.ApiResponses
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

/** Provide functional with external client. */
@Tag(name = "External", description = "The external API.")

@RestController
class ExternalController(private val externalService: ExternalService) {

    /** Return years of manufacture from external client. */
    @Operation(summary = "Get years of manufacture from external client.")
    @ApiResponses(ApiResponse(responseCode = "200", description = "Data from external service received."))

    @GetMapping("/years")
    fun getYearsOfManufacture() = externalService.getYears()
}