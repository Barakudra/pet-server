package by.birukov.pet.service.impl

import by.birukov.pet.dto.BasicClientDto
import by.birukov.pet.entity.Client
import by.birukov.pet.repository.ClientRepository
import by.birukov.pet.transformer.ClientTransformer
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.Test
import org.springframework.security.crypto.password.PasswordEncoder
import java.time.LocalDate
import kotlin.test.assertTrue

internal class ClientServiceImplTest {

    private val clientRepository = mockk<ClientRepository>()
    private val clientTransformer = mockk<ClientTransformer>()
    private val passwordEncoder = mockk<PasswordEncoder>()

    var clientServiceImpl = ClientServiceImpl(
        clientRepository,
        clientTransformer,
        passwordEncoder,
    )

    @Test
    fun getAll() {
        val entity = createEntity()
        val dto = createDto()
        val clientsEntities = mutableListOf(entity)

        every {
            clientRepository.findAll()
        } returns clientsEntities

        every {
            clientTransformer.transformEntityToBasicClientDto(entity)
        } returns dto

        val result: MutableList<BasicClientDto> = clientServiceImpl.getAll()

        assertTrue(result.size > 0)

        verify(exactly = 1) {
            clientRepository.findAll()
            clientServiceImpl.getAll()
        }
    }

    private fun createEntity() = Client(
        email = "",
        password = "",
        birthDate = LocalDate.now(),
        firstName = "",
        lastName = ""
    )

    private fun createDto() = BasicClientDto(
        email = "",
        lastName = ""
    )
}