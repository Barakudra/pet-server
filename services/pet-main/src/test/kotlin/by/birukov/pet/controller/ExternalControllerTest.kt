package by.birukov.pet.controller

import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.security.test.context.support.WithAnonymousUser
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.result.MockMvcResultHandlers
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@SpringBootTest
@AutoConfigureMockMvc
internal class ExternalControllerTest {

    @Autowired
    lateinit var mockMvc: MockMvc

    @Test
    @WithAnonymousUser
    fun getYearsOfManufacture() {
        mockMvc.perform(get(BASE_URL))
            .andDo(MockMvcResultHandlers.print())
            .andExpect(status().isOk)
    }

    companion object {
        const val BASE_URL = "http://localhost:8080/years"
    }
}