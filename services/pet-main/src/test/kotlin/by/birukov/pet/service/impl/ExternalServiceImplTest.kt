package by.birukov.pet.service.impl

import by.birukov.pet.client.ExternalClient
import by.birukov.pet.dto.ExternalYearContentDto
import by.birukov.pet.dto.ExternalYearDto
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.Test
import kotlin.test.assertTrue

internal class ExternalServiceImplTest {

    private val externalClient = mockk<ExternalClient>()

    private val externalServiceImpl = ExternalServiceImpl(externalClient)

    @Test
    fun getYears() {

        every {
            externalClient.getDataFromExternal()
        } returns ExternalYearDto("", 1, "", listOf(ExternalYearContentDto(200, 1995)))

        val result: List<Int> = externalServiceImpl.getYears()

        assertTrue(result.isNotEmpty())

        verify(exactly = 1) {
            externalClient.getDataFromExternal()
        }
    }
}