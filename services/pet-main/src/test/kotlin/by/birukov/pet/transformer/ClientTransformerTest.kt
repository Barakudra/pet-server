package by.birukov.pet.transformer

import by.birukov.pet.dto.ClientDetailsDto
import by.birukov.pet.entity.Client
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import java.time.LocalDate
import java.time.LocalDateTime

internal class ClientTransformerTest {

    private val clientTransformer = ClientTransformer()

    val client = Client(
        email = "abdylla@gmail.com",
        password = "twtwtrty",
        birthDate = LocalDate.of(2007, 9, 3),
        firstName = "Emma",
        lastName = "Oliv"
    ).apply {
        createdAt = LocalDateTime.of(2000, 1, 1, 1, 1)
        updatedAt = LocalDateTime.of(2000, 1, 1, 1, 1)
        id = 15
    }

    private val clientDetailsDto = ClientDetailsDto(
        email = "abdylla@gmail.com",
        password = "twtwtrty",
        birthDate = LocalDate.of(2007, 9, 3),
        firstName = "Emma",
        lastName = "Oliv",
        id = 15,
    ).apply {
        createdAt = LocalDateTime.of(2000, 1, 1, 1, 1)
        updatedAt = LocalDateTime.of(2000, 1, 1, 1, 1)
    }

    @Test
    fun transform() {
        val result = clientTransformer.transform(clientDetailsDto)

        assertEquals(client.password, result.password)
        assertEquals(client.email, result.email)
        assertEquals(client.birthDate, result.birthDate)
        assertEquals(client.firstName, result.firstName)
        assertEquals(client.createdAt, result.createdAt)
        assertEquals(client.updatedAt, result.updatedAt)
        assertEquals(client.id, result.id)
    }

    @Test
    fun testTransform() {
        val result = clientTransformer.transform(client)

        assertEquals(clientDetailsDto.email, result.email)
        assertEquals(clientDetailsDto.password, result.password)
        assertEquals(clientDetailsDto.birthDate, result.birthDate)
        assertEquals(clientDetailsDto.firstName, result.firstName)
        assertEquals(clientDetailsDto.createdAt, result.createdAt)
        assertEquals(clientDetailsDto.updatedAt, result.updatedAt)
        assertEquals(clientDetailsDto.id, result.id)
    }
}