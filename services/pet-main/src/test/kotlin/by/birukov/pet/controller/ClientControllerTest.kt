package by.birukov.pet.controller

import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.security.test.context.support.WithAnonymousUser
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.get

@SpringBootTest
@AutoConfigureMockMvc
internal class ClientControllerTest {

    @Autowired
    lateinit var mockMvc: MockMvc

    @Test
    @WithAnonymousUser
    fun getAll() {
        val res = mockMvc.get(BASE_URL)
            .andExpect {
                status { isOk() }
            }
        println(res.toString())
    }

    companion object {
        const val BASE_URL = "http://localhost:8080/users/all"
    }
}