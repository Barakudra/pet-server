package by.birukov.petauthorization.service

import by.birukov.petauthorization.client.UserAuthenticationClient
import by.birukov.petauthorization.constants.SecurityConstants.Companion.SECRET
import by.birukov.petauthorization.constants.SecurityConstants.Companion.TOKEN_PREFIX
import by.birukov.petauthorization.constants.SecurityConstants.Companion.EXPIRATION_TIME
import by.birukov.petauthorization.dto.AuthenticationRequestDto
import by.birukov.petauthorization.dto.AuthenticationResponseDto
import by.birukov.petauthorization.exception.WrongPasswordOrLoginException
import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.userdetails.User
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler
import org.springframework.stereotype.Service
import java.nio.charset.StandardCharsets
import java.util.Date
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Service
class ClientAuthServiceImpl(
    private val userAuthenticationClient: UserAuthenticationClient,
    private val authenticationManager: AuthenticationManager
) : ClientAuthService {

    override fun authenticate(creds: AuthenticationRequestDto): AuthenticationResponseDto {

        val authResult =
            authenticationManager.authenticate(UsernamePasswordAuthenticationToken(creds.email, creds.password))

        return userAuthenticationClient.auth(creds.email)?.let {

            val token: String = JWT.create()
                .withSubject((authResult?.principal as User).username)
                .withExpiresAt(Date(System.currentTimeMillis() + EXPIRATION_TIME))
                .sign(Algorithm.HMAC512(SECRET.toByteArray(StandardCharsets.UTF_8)))

            AuthenticationResponseDto(
                TOKEN_PREFIX + token,
                it.email,
                it.firstName,
                it.lastName,
            )
        } ?: throw WrongPasswordOrLoginException(WRONG_PASSWORD_OR_LOGIN_EXCEPTION)
    }

    override fun logout(request: HttpServletRequest, response: HttpServletResponse) =
        SecurityContextLogoutHandler().logout(request, response, null)

    companion object {
        private const val WRONG_PASSWORD_OR_LOGIN_EXCEPTION: String = "Wrong password or login"
    }
}