package by.birukov.petauthorization.exception

/** Info error message with key of exception, which must translate at client's side  */
class BusinessException(message: String?) : RuntimeException(message)