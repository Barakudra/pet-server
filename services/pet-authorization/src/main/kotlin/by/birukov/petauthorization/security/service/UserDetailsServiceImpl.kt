package by.birukov.petauthorization.security.service

import by.birukov.petauthorization.client.UserAuthenticationClient
import by.birukov.petauthorization.exception.WrongPasswordOrLoginException
import org.springframework.security.core.userdetails.User
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.stereotype.Service

/** Spring security feature that get user at username and try authenticate it */
@Service
class UserDetailsServiceImpl(private val userAuthenticationClient: UserAuthenticationClient) : UserDetailsService {

    override fun loadUserByUsername(email: String): UserDetails =
        userAuthenticationClient.auth(email)?.let {
            User(
                it.email,
                it.password,
                emptyList()
            )
        } ?: throw WrongPasswordOrLoginException(WRONG_EMAIL_OR_PASSWORD)

    companion object {
        private const val WRONG_EMAIL_OR_PASSWORD: String = "Wrong email or password"
    }
}