package by.birukov.petauthorization.dto

import java.io.Serializable

/** Response dto after success authorization with token and several info fields  */
data class AuthenticationResponseDto(
    val token: String,
    val email: String,
    val firstName: String,
    val lastName: String,
) : Serializable {

    companion object {
        private const val serialVersionUID: Long = 99900591086637541L
    }
}
