package by.birukov.petauthorization.controller

import by.birukov.petauthorization.dto.AuthenticationRequestDto
import by.birukov.petauthorization.dto.AuthenticationResponseDto
import by.birukov.petauthorization.service.ClientAuthService
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

/**
 * @author Birukov
 * Controller that must provide functional authentication and logout client
 */
@RestController
@RequestMapping("/auth")
class AuthController(private val clientAuthService: ClientAuthService) {

    @GetMapping("/login")
    fun auth(@RequestBody user: AuthenticationRequestDto): AuthenticationResponseDto =
        clientAuthService.authenticate(user)

    @GetMapping("/logout")
    fun logout(request: HttpServletRequest, response: HttpServletResponse): Unit =
        clientAuthService.logout(request, response)
}