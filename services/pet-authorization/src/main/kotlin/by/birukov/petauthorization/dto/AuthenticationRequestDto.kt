package by.birukov.petauthorization.dto

import java.io.Serializable

/** Authentication creds which authorize client */
data class AuthenticationRequestDto(
    val email: String,
    val password: String,
) : Serializable {

    companion object {
        private const val serialVersionUID: Long = 8275900591086637515L
    }
}
