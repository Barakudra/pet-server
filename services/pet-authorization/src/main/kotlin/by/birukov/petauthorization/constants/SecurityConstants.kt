package by.birukov.petauthorization.constants

/** Constants for Spring Security */
class SecurityConstants {

    companion object {
        const val EXPIRATION_TIME: Long = 864_000_000
        const val SECRET: String = "SECRET_WORD"
        const val HEADER: String = "Authorization"
        const val TOKEN_PREFIX: String = "Bearer_"
    }
}