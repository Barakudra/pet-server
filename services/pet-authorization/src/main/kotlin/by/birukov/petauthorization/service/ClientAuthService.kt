package by.birukov.petauthorization.service

import by.birukov.petauthorization.dto.AuthenticationRequestDto
import by.birukov.petauthorization.dto.AuthenticationResponseDto
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

/** Service for work with clients authentications */
interface ClientAuthService {

    /** Method thy authenticate client and return dto with token in basic case  */
    fun authenticate(creds: AuthenticationRequestDto): AuthenticationResponseDto

    /** Method provide logout and invalidate session */
    fun logout(request: HttpServletRequest, response: HttpServletResponse): Unit
}