package by.birukov.petauthorization.exception

/** Exception provide message about wrong creds */
class WrongPasswordOrLoginException(message: String?) : RuntimeException(message)