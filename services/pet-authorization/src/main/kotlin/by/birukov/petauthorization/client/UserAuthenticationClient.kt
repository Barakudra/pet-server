package by.birukov.petauthorization.client

import by.birukov.petauthorization.dto.ClientDetailsDto
import org.springframework.cloud.openfeign.FeignClient
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam

/** Provide data from another microservice */
@FeignClient(
    name = "pet-authorization",
    url = "http://localhost:8080/",
)
@RequestMapping("/users")
interface UserAuthenticationClient {

    /** Send request to url, return client details from general service  */
    @GetMapping("/authentication")
    fun auth(@RequestParam email: String): ClientDetailsDto?
}