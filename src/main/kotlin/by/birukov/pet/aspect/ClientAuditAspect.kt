package by.birukov.pet.aspect

import by.birukov.pet.dto.ClientDetailsDto
import by.birukov.pet.entity.AuditTrailEvent
import by.birukov.pet.entity.Client
import by.birukov.pet.repository.AuditRepository
import org.aspectj.lang.JoinPoint
import org.aspectj.lang.annotation.AfterReturning
import org.aspectj.lang.annotation.Aspect
import org.aspectj.lang.annotation.Pointcut
import org.springframework.stereotype.Component

@Aspect
@Component
class ClientAuditAspect(private val auditRepository: AuditRepository) {

    @Pointcut("execution(* by.birukov.pet.service.ClientService.update(..))")
    fun updateClient() = Unit

    @Pointcut("execution(* by.birukov.pet.service.ClientService.create(..))")
    fun createClient() = Unit

    @AfterReturning(value = "updateClient()", returning = "clientDetailsDto")
    fun afterUpdateClient(joinPoint: JoinPoint, clientDetailsDto: ClientDetailsDto) {
        val auditTrailEvent = AuditTrailEvent(
            TYPE_CLIENT,
            clientDetailsDto.id,
            ACTION_UPDATE
        )
        auditRepository.save(auditTrailEvent)
    }

    @AfterReturning(value = "createClient()", returning = "registeredClient")
    fun afterCreateClient(joinPoint: JoinPoint, registeredClient: ClientDetailsDto) {
        val auditTrailEvent = AuditTrailEvent(
            TYPE_CLIENT,
            registeredClient.id,
            ACTION_CREATE
        )
        auditRepository.save(auditTrailEvent)
    }

    companion object {
        private const val TYPE_CLIENT: String = "CLIENT"
        private const val ACTION_UPDATE: String = "UPDATE"
        private const val ACTION_CREATE: String = "CREATE"
    }
}